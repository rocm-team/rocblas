rocblas (5.5.1+dfsg-7) unstable; urgency=medium

  * Migrate to unstable.
  * Update standards version to 4.7.0. No changes required.
  * Update upstream URLs.

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 20 Nov 2024 01:27:40 -0700

rocblas (5.5.1+dfsg-7~exp1) experimental; urgency=medium

  * Add librocblas0-bench package.
  * Add d/p/0025-spelling.patch to fix spelling error in rocblas-bench.
  * d/rules: fix path to rocblas-test in build tests
  * d/rules: reduce build tests to a reasonable subset of autopkgtests
    so that maintainers actually run them.

 -- Cordell Bloor <cgmb@slerp.xyz>  Thu, 31 Oct 2024 15:14:32 -0600

rocblas (5.5.1+dfsg-6) unstable; urgency=medium

  * Add d/p/0023-remove-mf16c-flag.patch to fix compatibility with non-AVX
    processors. (Closes: #1075724)
  * d/rules: simplify target architectures to match other ROCm libraries,
    dropping xnack- specialization on gfx906, gfx908 and xnack-/xnack+
    specialization on gfx90a (except in tensile kernels).
  * Add d/p/0024-use-xnack-specialized-assembly-kernels-with-gfx90a.patch
    to retain xnack-/xnack+ specialization in tensile kernels.
  * d/gbp.conf: ensure use of pristine-tar

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 03 Jul 2024 15:23:54 -0600

rocblas (5.5.1+dfsg-5) unstable; urgency=medium

  * Migrate to unstable. (Closes: #1066792)
  * Add d/p/0022-reserved-identifiers.patch to backport fix for
    reserved identifiers in rocblas headers.
  * Use llvm-objdump from llvm-17 rather than from llvm-16.

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 13 Mar 2024 10:14:41 -0600

rocblas (5.5.1+dfsg-5~exp2) experimental; urgency=medium

  [ Cordell Bloor ]
  * d/rules: skip rocblas-test in nocheck build profile
  * d/rules: skip building test data in nocheck build profile

  [ Christian Kastner ]
  * d/control: Add missing Breaks+Replaces for moved examples.
    They are now part of the -doc package. (Closes: #1065351)
  * Add lintian override for copyright false positive

 -- Christian Kastner <ckk@debian.org>  Sun, 03 Mar 2024 12:18:28 +0100

rocblas (5.5.1+dfsg-5~exp1) experimental; urgency=medium

  * Enable gfx1100, gfx1101, and gfx1102 architectures
  * The dev package now suggests the doc package
  * Code examples have been moved from the dev package to doc package
  * Removed patchelf from the Build-Depends
  * Use compressed DWARF5 debug symbols
  * d/control: mark test packages as <!nocheck>
  * d/control: add llvm to Build-Depends to workaround Bug #1065097

 -- Cordell Bloor <cgmb@slerp.xyz>  Thu, 29 Feb 2024 15:43:38 -0700

rocblas (5.5.1+dfsg-4) unstable; urgency=medium

  [ Christian Kastner ]
  * autopkgtest: Export dmesg and other info as artifacts

  [ James McCoy ]
  * Switch libmsgpack-dev to libmsgpack-cxx-dev. (Closes: #1061623)

 -- Christian Kastner <ckk@debian.org>  Fri, 02 Feb 2024 14:18:29 +0100

rocblas (5.5.1+dfsg-3) unstable; urgency=medium

  * Add myself to Uploaders
  * autopkgtest: Use libopenblas0-pthread BLAS to speed up tests.
  * Filter cf-protection hardening from device code.
    Fixes a FTBFS with dpkg >= 1.22

 -- Christian Kastner <ckk@debian.org>  Tue, 19 Sep 2023 10:28:14 +0200

rocblas (5.5.1+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * Fix ftbfs-source-after-build. (Closes: #1046971)

 -- Christian Kastner <ckk@debian.org>  Thu, 17 Aug 2023 20:11:43 +0200

rocblas (5.5.1+dfsg-2~exp1) experimental; urgency=medium

  * Add librocblas-doc package providing library documentation.
  * Add librocblas0-tests and librocblas0-tests-data packages
    for autopkgtests.
  * Add patches to fix build errors on arm64 and ppc64el.

 -- Cordell Bloor <cgmb@slerp.xyz>  Fri, 28 Jul 2023 15:08:48 -0600

rocblas (5.5.1+dfsg-1) unstable; urgency=medium

  * Migrate to unstable.
  * Fix maintainer name.
  * Restore d/p/0013-disable-rotg-nan-check.patch to fix tests.
  * Remove stray references to missing replacement kernels on
    gfx906, gfx908 and gfx90a (Closes: #1042036).

 -- Cordell Bloor <cgmb@slerp.xyz>  Thu, 27 Jul 2023 09:53:26 -0600

rocblas (5.5.1+dfsg-1~exp1) experimental; urgency=medium

  * d/rules: explicitly disable lto
  * New upstream version 5.5.1+dfsg

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 28 Jun 2023 17:23:22 -0600

rocblas (5.3.3+dfsg-2) unstable; urgency=medium

  * Migrate to unstable.
  * Add d/p/0012-expand-isa-compatibility.patch to enable experimental
    support for gfx902, gfx909, gfx90c, gfx1011, gfx1031, gfx1032,
    gfx1033, gfx1034, gfx1035 and gfx1036 when combined with
    rocr-runtime 5.2.3-4~exp1 and rocm-hipamd 5.2.3-10~exp1.
  * Add 0011-disable-stdc-extension-in-header.patch to fix inconsistent
    behaviour depending on order of inclusion of rocblas headers.
  * Add d/p/0013-disable-rotg-nan-check.patch and
    d/p/0014-fixup-backported-tests.patch to fix rotg tests.
  * d/copyright: fix typo

 -- Cordell Bloor <cgmb@slerp.xyz>  Sat, 03 Jun 2023 10:40:57 -0600

rocblas (5.3.3+dfsg-1~exp1) experimental; urgency=medium

  * Initial packaging (Closes: #1022871)

 -- Cordell Bloor <cgmb@slerp.xyz>  Fri, 05 May 2023 01:48:50 -0600
