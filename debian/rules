#!/usr/bin/make -f
export CXX=hipcc
export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=-lto
export DEB_CXXFLAGS_MAINT_PREPEND = -gz
export VERBOSE=1
export TENSILE_ROCM_ASSEMBLER_PATH=/usr/bin/clang++-17
export TENSILE_ROCM_OFFLOAD_BUNDLER_PATH=/usr/bin/clang-offload-bundler-17
export PATH:=$(PATH):/usr/lib/llvm-17/bin
#export AMD_LOG_LEVEL=4

CXXFLAGS := $(subst -fstack-protector-strong,-Xarch_host -fstack-protector-strong,$(CXXFLAGS))
CXXFLAGS := $(subst -fcf-protection,-Xarch_host -fcf-protection,$(CXXFLAGS))

CMAKE_FLAGS = \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_SKIP_INSTALL_RPATH=ON \
	-DROCM_SYMLINK_LIBS=OFF \
	-DBUILD_CLIENTS_BENCHMARKS=ON \
	-DBUILD_FILE_REORG_BACKWARD_COMPATIBILITY=OFF \
	-DAMDGPU_TARGETS="gfx803;gfx900;gfx906;gfx908;gfx90a;gfx1010;gfx1030;gfx1100;gfx1101;gfx1102" \
	-DROCBLAS_TENSILE_LIBRARY_DIR=/usr/lib/$(DEB_HOST_MULTIARCH)/rocblas/2.47.0 \
	-DTensile_TEST_LOCAL_PATH="../tensile" \
	-DTensile_LOGIC=asm_full \
	-DTensile_LIBRARY_FORMAT=msgpack \
	-DTensile_LAZY_LIBRARY_LOADING=OFF \
	-DTensile_SEPARATE_ARCHITECTURES=OFF \
	-DRUN_HEADER_TESTING=OFF

ifeq (,$(filter nocheck,$(DEB_BUILD_PROFILES)))
CMAKE_FLAGS += \
	-DBUILD_CLIENTS_TESTS=ON
endif

%:
	dh $@ -Scmake

override_dh_auto_configure-arch:
	dh_auto_configure -- $(CMAKE_FLAGS)

# strip cannot handle code object files
override_dh_strip-arch:
	dh_strip --exclude=.hsaco --exclude=.co

override_dh_dwz-arch:
	:

# note: openblas crashes in dot for complex values with M=128, N=800000, incx=-3, incy=-3
override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	set -e \
	; if [ -r /dev/kfd ] \
	; then obj-$(DEB_HOST_GNU_TYPE)/clients/staging/rocblas-test \
	    --gtest_filter="*checkin*:-*known_bug*" \
	; else echo "W: /dev/kfd unreadable: no available AMD GPU." \
	;      echo "W: tests skipped." \
	; fi
endif

override_dh_auto_configure-indep:
	:

override_dh_auto_build-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	cd docs; doxygen
	sphinx-build -b html docs/source html
endif
ifeq (,$(filter nocheck,$(DEB_BUILD_PROFILES)))
	python3 clients/common/rocblas_gentest.py -I clients/include clients/gtest/rocblas_gtest.yaml -o rocblas_gtest.data
endif

override_dh_auto_test-indep:
	:

override_dh_auto_install-indep:
	:
